package jcommander;

import javafx.concurrent.Task;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

/**
 * Created by user on 2017-04-27.
 */
public class FilesOperations {
    public static Task delete(String path) {
        return ExecuteOperation(OperationType.DELETE, path, "");
    }//detele()

    public static Task copy(String target, String destination) {
        return ExecuteOperation(OperationType.COPY, target, destination);
    }

    private static Task ExecuteOperation(OperationType operation, String path, String destination) {
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    switch (operation) {
                        case DELETE: {
                            DirectoryModel directoryModel = new DirectoryModel(path, this);
                            deleteAll(directoryModel, this);
                            break;
                        }//case
                    }//switch
                } catch (IOException e) {
                    updateMessage(ResourceService.getResourceBundle().getString("message.deleteError")
                            + e.getMessage() + "\n"
                            + ResourceService.getResourceBundle().getString("message.IOError"));
                }//catch()
                return null;
            }
        };
    }//ExecuteOperation()

    private static void deleteAll(DirectoryModel directoryModel, Task task) throws IOException {
        System.out.println("DeleteAll");
        if(task.isCancelled())
            return;
        if (!directoryModel.IS_FILE) {
            for (DirectoryModel dm :
                    directoryModel.SUBDIRECTORIES) {
                deleteAll(dm, task);
            }//for
        }//if

        //System.out.println("Deleting "+directoryModel.PATH);
        File file = new File(directoryModel.PATH);
        if (!file.delete()) {
            System.err.println("Error while deleting " + directoryModel.PATH);
            throw new IOException(directoryModel.PATH);
        }//if
    }//deleteAll()
}
