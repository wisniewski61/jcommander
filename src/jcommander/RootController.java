package jcommander;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class RootController {
    @FXML
    private ComboBox firstCombobox, secondCombobox;
    @FXML
    private TableView<TableViewModel> tableView1, tableView2;
    @FXML
    private TextField firstPath, secondPath;
    @FXML
    private TableColumn<TableViewModel, String> nameColumn1, nameColumn2, sizeColumn1, sizeColumn2, createdColumn1, createdColumn2;
    @FXML
    private Menu fileMenu, languageMenu, helpMenu;
    @FXML
    private MenuItem closeMenuItem, polishMenuItem, englishMenuItem, helpMenuItem;
    @FXML
    private Button copyButton, moveButton, deleteButton;

    private TableViewModel lastSelectedItem;
    private TextField lastSelectedPathField;

    private ResourceService resources;

    @FXML
    public void initialize() {
        resources = new ResourceService();
        ArrayList drives = getDrives();
        initComboboxAndPath(firstCombobox, drives, firstPath);
        initComboboxAndPath(secondCombobox, drives, secondPath);

        initTableView(tableView1, firstPath, nameColumn1, sizeColumn1, createdColumn1);
        initTableView(tableView2, secondPath, nameColumn2, sizeColumn2, createdColumn2);

        // source: https://stackoverflow.com/questions/26424769/javafx8-how-to-create-listener-for-selection-of-row-in-tableview
        tableView1.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                tableView2.getSelectionModel().clearSelection();
                lastSelectedItem = newSelection;
                lastSelectedPathField = firstPath;
            }
        });

        tableView2.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                tableView1.getSelectionModel().clearSelection();
                lastSelectedItem = newSelection;
                lastSelectedPathField = secondPath;
            }
        });
    }//initialize()

    private void initTableView(TableView tableView, TextField pathField,
                               TableColumn<TableViewModel, String> nameColumn,
                               TableColumn<TableViewModel, String> sizeColumn,
                               TableColumn<TableViewModel, String> createdColumn) {
        FilesAndDirectoriesModel filesAndDirectories = getFilesAndDirectories(pathField.getText());
        ArrayList<TableViewModel> files = filesAndDirectories.getFilesViewModels();
        ArrayList<TableViewModel> dirs = filesAndDirectories.getDirectoriesViewModels();

        files.addAll(dirs);
        tableView.getItems().addAll(files);
        nameColumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().NAME));
        sizeColumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().SIZE));
        sizeColumn.setComparator(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                Long l1 = o1.equals("<DIR>") ? 0 : Long.parseLong(o1);
                Long l2 = o2.equals("<DIR>") ? 0 : Long.parseLong(o2);
                return l1 == l2 ? 0 : l1 < l2 ? -1 : 1;
            }
        });
        createdColumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().CREATED));
    }//initTableView()

    private FilesAndDirectoriesModel getFilesAndDirectories(String path) {
        File folder = new File(path);
        File[] itemsList = folder.listFiles();
        ArrayList<File> directories = new ArrayList<>();
        ArrayList<File> files = new ArrayList<>();

        for (File item : itemsList) {
            if (item.isFile()) {
                files.add(item);
            } else if (item.isDirectory()) {
                directories.add(item);
            }//else
        }//for

        return new FilesAndDirectoriesModel(files, directories);
    }//getFilesAndDirectories()

    private void initComboboxAndPath(ComboBox comboBox, ArrayList<String> drives, TextField pathField) {
        comboBox.getItems().addAll(drives);
        comboBox.getSelectionModel().select(0);
        pathField.insertText(0, comboBox.getSelectionModel().getSelectedItem().toString());
        comboBox.getSelectionModel().selectedItemProperty().addListener(new ComboboxListener(comboBox, pathField));
    }//initComboboxAndPath()

    private ArrayList<String> getDrives() {
        File[] drives = File.listRoots();
        ArrayList<String> drivesList = new ArrayList<>();

        for (File path : drives)
            drivesList.add(path.toString());

        return drivesList;
    }//getDrives()

    public void pathActionHandler(ActionEvent event) {
        TextField tf = (TextField) event.getSource();
        ComboBox comboBox = firstCombobox;
        TableView<TableViewModel> tv = tableView1;
        TextField pathField = firstPath;
        TableColumn<TableViewModel, String> nameCol = nameColumn1, sizeCol = sizeColumn1, createdCol = createdColumn1;
        if (tf.equals(secondPath)) {
            comboBox = secondCombobox;
            tv = tableView2;
            pathField = secondPath;
            nameCol = nameColumn2;
            sizeCol = sizeColumn2;
            createdCol = createdColumn2;
        }//if

        String[] pathElems = tf.getText().split(Pattern.quote("\\"));
        String pathStr = pathElems[0] + "\\";
        if (!comboBox.getItems().contains(pathStr)) {
            tf.clear();
            tf.setText(comboBox.getSelectionModel().getSelectedItem().toString());
            return;
        }//if

        tv.getItems().clear();
        initTableView(tv, pathField, nameCol, sizeCol, createdCol);
        comboBox.getSelectionModel().select(pathStr);
    }//pathActionHandler()

    public void tableViewKeyHandler(KeyEvent keyEvent) {
        TableView<TableViewModel> tv = (TableView) keyEvent.getSource();
        TextField pathField = firstPath;
        if (tv.equals(tableView2))
            pathField = secondPath;

        if (keyEvent.getCode() == KeyCode.ENTER) {
            tvEnterPressed(tv, pathField);
            return;
        }//if
        if (keyEvent.getCode() == KeyCode.BACK_SPACE) {
            tvBackspacePressed(pathField);
            return;
        }//if
    }//tableViewKeyHandler()

    private void tvBackspacePressed(TextField pathField) {
        String[] pathElems = pathField.getText().split(Pattern.quote("\\"));
        if (pathElems.length == 1)
            return;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < pathElems.length - 1; i++) {
            sb.append(pathElems[i]).append("\\");
        }

        String newPath = sb.toString();
        File item = new File(newPath);
        if (!item.isDirectory())
            return;

        pathField.clear();
        pathField.setText(sb.toString());
        pathField.fireEvent(new ActionEvent());
    }//tvBackspacePressed()

    private void tvEnterPressed(TableView<TableViewModel> tv, TextField pathField) {
        String[] pathElems = pathField.getText().split(Pattern.quote("\\"));
        StringBuilder sb = new StringBuilder();
        for (String pathElement :
                pathElems) {
            sb.append(pathElement).append("\\");
        }
        sb.append(tv.getFocusModel().getFocusedItem().NAME);

        String newPath = sb.toString();
        File item = new File(newPath);
        if (!item.isDirectory())
            return;

        pathField.clear();
        pathField.setText(sb.toString());
        pathField.fireEvent(new ActionEvent());
    }//tvEnterPressed()

    public void tableViewMouseHandler(MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() > 1) {
            TableView<TableViewModel> tv = (TableView) mouseEvent.getSource();
            TextField pathField = firstPath;
            if (tv.equals(tableView2))
                pathField = secondPath;
            tvEnterPressed(tv, pathField);
        }//if
    }//tableViewMouseHandler()

    public void copyPressed(ActionEvent actionEvent) {
        String title = ResourceService.getResourceBundle().getString("bottomMenu.copy");
        String label = ResourceService.getResourceBundle().getString("progress.copying");
        copyOrMove(OperationType.COPY, title, label);
    }

    private void copyOrMove(OperationType operationType, String title, String label) {
        TextField target = firstPath;
        TextField pathField = lastSelectedPathField;
        if (pathField.equals(firstPath))
            target = secondPath;

        FileOperationsThread fot = new FileOperationsThread(operationType,
                pathField.getText() + "\\" + lastSelectedItem.NAME,
                target.getText());
        fot.conflictMessageProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Value changed on: "+newValue);
                        Alert alert = createAndShowAlert(ResourceService.getResourceBundle().getString("conflict"), newValue, ButtonType.YES, ButtonType.NO);
                        ButtonType result = alert.getResult();
                        if(result==ButtonType.YES)
                            fot.retry(true);
                        else if(result==ButtonType.NO)
                            fot.retry(false);
                    }
                });
            }
        });

        Parent root;
        Stage stage = new Stage();
        FXMLLoader fxmlLoader;
        try {
            fxmlLoader = new FXMLLoader(getClass().getResource("progress.fxml"));
            root = fxmlLoader.load();
            stage.setTitle(title);
            stage.setScene(new Scene(root));
            FileWorkStatusHandler handler = new FileWorkStatusHandler(pathField, stage);
            fxmlLoader.<ProgressController>getController().setFileOperationsThread(fot);
            fxmlLoader.<ProgressController>getController().setInfoLabel(label);
            fot.resultProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    if(newValue.intValue()==-1) {
                        String message = fot.getResultMessage();
                        Alert alert = createAndShowAlert(ResourceService.getResourceBundle().getString("message.titleError"), message, ButtonType.OK);
                    } else {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                stage.close();
                                firstPath.fireEvent(new ActionEvent());
                                secondPath.fireEvent(new ActionEvent());
                            }
                        });
                        //pathField.fireEvent(new ActionEvent());
                        //target.fireEvent(new ActionEvent());
                    }
                }
            });
            new Thread(fot).start();
            stage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void movePressed(ActionEvent actionEvent) {
        String title = ResourceService.getResourceBundle().getString("bottomMenu.move");
        String label = ResourceService.getResourceBundle().getString("progress.moving");
        copyOrMove(OperationType.MOVE, title, label);
    }//if

    public void deletePressed(ActionEvent actionEvent) {
        TextField pathField = lastSelectedPathField;
        String[] pathElems = pathField.getText().split(Pattern.quote("\\"));
        StringBuilder sb = new StringBuilder();
        for (String pathElement :
                pathElems) {
            sb.append(pathElement).append("\\");
        }
        sb.append(lastSelectedItem.NAME);

        String title = resources.getResourceBundle().getString("bottomMenu.delete");
        Alert alert = createAndShowAlert(title, title + " " + lastSelectedItem.NAME + "?", ButtonType.YES, ButtonType.NO);

        if (alert.getResult() == ButtonType.YES) {
            Parent root;
            Stage stage = new Stage();
            FXMLLoader fxmlLoader;
            try {
                fxmlLoader = new FXMLLoader(getClass().getResource("progress.fxml"));
                root = fxmlLoader.load();
                stage.setTitle(title);
                stage.setScene(new Scene(root));
                Task deleteTask = FilesOperations.delete(sb.toString());
                FileWorkStatusHandler handler = new FileWorkStatusHandler(pathField, stage);
                deleteTask.setOnSucceeded(handler);
                deleteTask.setOnCancelled(handler);
                deleteTask.setOnFailed(handler);
                deleteTask.messageProperty().addListener(new ChangeListener<String>() {
                    @Override
                    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                        createAndShowAlert(ResourceService.getResourceBundle().getString("message.titleError"),
                                newValue, ButtonType.CLOSE);
                    }
                });

                Thread worker = new Thread(deleteTask);
                fxmlLoader.<ProgressController>getController().setWorkerThread(worker);
                fxmlLoader.<ProgressController>getController().setWorkerTask(deleteTask);
                fxmlLoader.<ProgressController>getController().setInfoLabel(ResourceService.getResourceBundle().getString("progress.deleting"));
                worker.start();
                stage.show();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }//if
    }//deletePressed

    private Alert createAndShowAlert(String title, String content, ButtonType... buttons) {
        Alert alert = new Alert(Alert.AlertType.NONE, content, buttons);
        alert.setTitle(title);
        alert.showAndWait();

        return alert;
    }//createAndShowAlert()

    public void polish(ActionEvent actionEvent) {
        ResourceService.setLocale(new Locale("pl"));
        changeLanguage();
    }

    public void english(ActionEvent actionEvent) {
        ResourceService.setLocale(new Locale("en"));
        changeLanguage();
    }

    private void changeLanguage() {
        ResourceBundle rb = ResourceService.getResourceBundle();
        fileMenu.setText(rb.getString("file"));
        languageMenu.setText(rb.getString("language"));
        helpMenu.setText(rb.getString("help"));
        closeMenuItem.setText(rb.getString("close"));
        polishMenuItem.setText(rb.getString("language.polish"));
        englishMenuItem.setText(rb.getString("language.english"));
        helpMenuItem.setText(rb.getString("help.about"));
        nameColumn1.setText(rb.getString("fileTable.name"));
        nameColumn2.setText(rb.getString("fileTable.name"));
        sizeColumn1.setText(rb.getString("fileTable.size"));
        sizeColumn2.setText(rb.getString("fileTable.size"));
        createdColumn1.setText(rb.getString("fileTable.creationDate"));
        createdColumn2.setText(rb.getString("fileTable.creationDate"));
        copyButton.setText(rb.getString("bottomMenu.copy"));
        moveButton.setText(rb.getString("bottomMenu.move"));
        deleteButton.setText(rb.getString("bottomMenu.delete"));

        firstPath.fireEvent(new ActionEvent());
        secondPath.fireEvent(new ActionEvent());
    }
}
