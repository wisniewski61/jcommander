package jcommander;

import javafx.concurrent.Task;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by user on 2017-04-30.
 */
public class DirectoryModel {
    //public int DEPTH;
    public String PATH;
    public boolean IS_FILE;
    public ArrayList<DirectoryModel> SUBDIRECTORIES;
    public long SIZE;
    private Task task;

    public DirectoryModel(String path, Task task) {
        if(task.isCancelled())
            return;
        this.task = task;
        PATH = path;
        File file = new File(path);
        IS_FILE = file.isFile();
        if(!IS_FILE) {
            SUBDIRECTORIES = new ArrayList<>();
            File[] subfilesList = file.listFiles();
            for (File subfile :
                    subfilesList) {
                DirectoryModel subfileModel = new DirectoryModel(subfile.getAbsolutePath(), task);
                SUBDIRECTORIES.add(subfileModel);
                SIZE += subfileModel.SIZE;
            }//for
        } else {
            SIZE = file.getTotalSpace();
        }//else
    }//DirectoryModel()

    DirectoryModel(String path, AtomicBoolean isCancelled) {
        if(isCancelled.get())
            return;
        PATH = path;
        File file = new File(path);
        IS_FILE = file.isFile();
        if(!IS_FILE) {
            SUBDIRECTORIES = new ArrayList<>();
            File[] subfilesList = file.listFiles();
            for (File subfile :
                    subfilesList) {
                DirectoryModel subfileModel = new DirectoryModel(subfile.getAbsolutePath(), isCancelled);
                SUBDIRECTORIES.add(subfileModel);
                SIZE += subfileModel.SIZE;
            }//for
        } else {
            SIZE = file.getTotalSpace();
        }//else
    }
}
