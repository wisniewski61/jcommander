package jcommander;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

/**
 * Created by user on 2017-04-26.
 */
public class ComboboxListener implements ChangeListener<String> {
    private final ComboBox self;
    private final TextField pathField;

    ComboboxListener(ComboBox comboBox, TextField pathField) {
        self = comboBox;
        this.pathField = pathField;
    }//ComboboxListener()

    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        System.out.println(self.getId() + " value changed from "+ oldValue + " to "+ newValue);
        if(pathField.getText().startsWith(newValue))
            return;
        pathField.clear();
        pathField.insertText(0, newValue);
        pathField.fireEvent(new ActionEvent());
    }//changed()
}
