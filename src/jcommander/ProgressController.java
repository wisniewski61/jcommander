package jcommander;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.stage.Stage;

import java.util.ResourceBundle;

/**
 * Created by user on 2017-05-01.
 */
public class ProgressController {
    @FXML
    private ProgressBar progressBar;

    @FXML
    private Label infoLabel;

    @FXML
    private Button cancelButton;

    private Thread workerThread;
    private Task task;
    private FileOperationsThread fileOperationsThread;

    @FXML
    public void initialize() {
        ResourceBundle rb = ResourceService.getResourceBundle();
        if(infoLabel.getText()==null)
            infoLabel.setText("");
        //infoLabel.setText(rb.getString("progress.deleting"));
        cancelButton.setText(rb.getString("progress.cancel"));
    }

    public void setWorkerThread(Thread workerThread) {
        this.workerThread = workerThread;
    }

    public void setWorkerTask(Task task) {
        this.task = task;
    }

    public void setFileOperationsThread(FileOperationsThread fileOperationsThread) {
        this.fileOperationsThread = fileOperationsThread;
    }

    public void cancelPressed(ActionEvent actionEvent) {
        if(task!=null)
            task.cancel();
        if(fileOperationsThread!=null)
            fileOperationsThread.cancel();
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }//cancelPressed()

    public void setInfoLabel(String text) {
        infoLabel.setText(text);
    }
}
