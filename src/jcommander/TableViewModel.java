package jcommander;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by user on 2017-04-27.
 */
public class TableViewModel {
    public String NAME;
    public String SIZE;
    public String CREATED;

    TableViewModel(String name, String size, String created) {
        NAME = name;
        SIZE = size;
        CREATED = created;
    }//TableViewModel()

    TableViewModel(File item) {
        NAME = item.getName();
        SIZE = item.isFile() ? fileSize(item.length()) : "<DIR>";
        Path pItem = Paths.get(item.toURI());
        try {
            String wholeDate = Files.readAttributes(pItem, BasicFileAttributes.class).creationTime().toString();
            String[] wholeDataSplited = wholeDate.split("T");
            CREATED = wholeDataSplited[0];
            wholeDataSplited = wholeDataSplited[1].split("\\.");
            CREATED += " " + wholeDataSplited[0];
        } catch(IOException ioe) {
            CREATED = "";
        }//catch
    }//TableViewModel()

    private String fileSize(long length) {
        return String.valueOf(length);
    }

    // source: https://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format-in-java
    public static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getSIZE() {
        return SIZE;
    }

    public void setSIZE(String SIZE) {
        this.SIZE = SIZE;
    }

    public String getCREATED() {
        return CREATED;
    }

    public void setCREATED(String CREATED) {
        this.CREATED = CREATED;
    }
}
