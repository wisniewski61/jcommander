package jcommander;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by user on 2017-04-30.
 */
public class ResourceService {
    private static ResourceBundle resourceBundle;
    private static Locale locale;
    private static String resourceAddress;

    public ResourceService(){}

    public ResourceService(String resourceAddress, Locale locale) {
        this.resourceAddress = resourceAddress;
        this.resourceBundle = ResourceBundle.getBundle(resourceAddress, locale);
        this.locale = locale;
    }

    public static ResourceBundle getResourceBundle() {
        return resourceBundle;
    }

    public static Locale getLocale() {
        return locale;
    }

    public static void setLocale(Locale locale) {
        ResourceService.locale = locale;
        resourceBundle = ResourceBundle.getBundle(resourceAddress, locale);
    }
}
