package jcommander;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 2017-04-27.
 */
public class FilesAndDirectoriesModel {
    public ArrayList<File> FILES;
    public ArrayList<File> DIRECTORIES;

    public FilesAndDirectoriesModel(ArrayList<File> files, ArrayList<File> directories) {
        FILES = files;
        DIRECTORIES = directories;
    }//FilesAndDirectoriesModel()

    private ArrayList<TableViewModel> getViewModels(ArrayList<File> items) {
        ArrayList<TableViewModel> itemsViewModels = new ArrayList<>();
        for(File item : items)
            itemsViewModels.add(new TableViewModel(item));

        return itemsViewModels;
    }//getViewModels()

    public ArrayList<TableViewModel> getFilesViewModels() {
        return getViewModels(FILES);
    }//getFilesViewModels()

    public ArrayList<TableViewModel> getDirectoriesViewModels() {
        return getViewModels(DIRECTORIES);
    }//getDirectoriesViewModels()
}
