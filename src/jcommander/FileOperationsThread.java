package jcommander;

import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by user on 2017-05-01.
 */
public class FileOperationsThread implements Runnable {
    private StringProperty conflictMessage;
    private OperationType operationType;
    private String target, destination;
    private AtomicBoolean waitingForUserResponse;
    private AtomicBoolean userResponse;
    private AtomicBoolean isCancelled;
    private IntegerProperty result;
    private String resultMessage;

    FileOperationsThread(OperationType operation, String targetPath, String destinationPath) {
        conflictMessage = new SimpleStringProperty(this, "confilctMessage");
        result = new SimpleIntegerProperty(this, "result");
        operationType = operation;
        target = targetPath;
        destination = destinationPath;
        waitingForUserResponse = new AtomicBoolean(false);
        userResponse = new AtomicBoolean(true);
        isCancelled = new AtomicBoolean(false);
    }//FileOperationsThread()

    @Override
    public void run() {
        File fTarget = new File(target);
        if(destination!=null) {
            File fDestination = new File(destination);
            if(fTarget.getParent().equals(destination))
                return; // ten sam katalog
            File[] destinationFiles = fDestination.listFiles();
            for (File f :
                    destinationFiles) {
                if (f.getName().equals(fTarget.getName())) {
                    waitingForUserResponse.set(true);
                    conflictMessage.set(ResourceService.getResourceBundle().getString("conflict.fileExists"));
                    break;
                }//if
            }//for
        }//if

        while (waitingForUserResponse.get()) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }//while
        if(!userResponse.get()) {
            result.set(1);
            return; //nie nadpisuj
        }
        try {
            DirectoryModel directoryModel = new DirectoryModel(target, isCancelled);
            copyOrMoveAll(directoryModel, destination, operationType);
        } catch (IOException e) {
            e.printStackTrace();
            String s = "message.copyError";
            if(operationType==OperationType.MOVE)
                s = "message.moveError";
            resultMessage = ResourceService.getResourceBundle().getString(s)+e.getMessage();
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    result.set(-1);
                }
            });
            return;
        }
        //conflictMessage.set("New value");
        result.set(1);
    }

    private void copyOrMoveAll(DirectoryModel directoryModel, String destination, OperationType operation) throws IOException {
        if(isCancelled.get())
            return;
        if(directoryModel.IS_FILE) {
            File f = new File(directoryModel.PATH);
            if(operation==OperationType.COPY)
                Files.copy(Paths.get(directoryModel.PATH), Paths.get(destination+"\\"+f.getName()), StandardCopyOption.REPLACE_EXISTING);
            else if(operation==OperationType.MOVE)
                Files.move(Paths.get(directoryModel.PATH), Paths.get(destination+"\\"+f.getName()), StandardCopyOption.REPLACE_EXISTING);
        } else {
            File f = new File(directoryModel.PATH);
            try {
                Files.createDirectory(Paths.get(destination + "\\" + f.getName()));
            } catch (FileAlreadyExistsException e) {}
            for (DirectoryModel dm :
                    directoryModel.SUBDIRECTORIES) {
                copyOrMoveAll(dm, destination+"\\"+f.getName(), operation);
            }//for
            if(operation==OperationType.MOVE)
                f.delete();
        }//else
    }//copyOrMoveAll()

    public void retry(boolean response) {
        userResponse.set(response);
        waitingForUserResponse.set(false);
    }//retry()

    public String getConflictMessage() {
        return conflictMessage.get();
    }

    public StringProperty conflictMessageProperty() {
        return conflictMessage;
    }

    public int getResult() {
        return result.get();
    }

    public IntegerProperty resultProperty() {
        return result;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void cancel() {
        this.isCancelled.set(true);
    }
}
