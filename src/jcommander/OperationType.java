package jcommander;

/**
 * Created by user on 2017-05-01.
 */
public enum OperationType {
    DELETE, COPY, MOVE
}
