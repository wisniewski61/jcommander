package jcommander;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.util.Locale;
import java.util.ResourceBundle;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("root.fxml"));
        //ResourceBundle.getBundle("jcommander.lang", new Locale("en"))
        Locale locale =  new Locale("en");
        ResourceService rs = new ResourceService("jcommander.lang", locale);
        fxmlLoader.setResources(rs.getResourceBundle());
        BorderPane root = fxmlLoader.load();
        primaryStage.setTitle("jcommander");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
