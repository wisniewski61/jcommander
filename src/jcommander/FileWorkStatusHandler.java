package jcommander;

import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Dialog;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Created by user on 2017-05-01.
 */
public class FileWorkStatusHandler implements EventHandler<WorkerStateEvent> {
    private TextField pathField;
    private Stage progressDialog;

    public FileWorkStatusHandler(TextField pathField, Stage progressDialog) {
        this.pathField = pathField;
        this.progressDialog = progressDialog;
    }

    @Override
    public void handle(WorkerStateEvent event) {
        System.out.println("Worker finished");
        progressDialog.close();
        pathField.fireEvent(new ActionEvent());
    }
}
